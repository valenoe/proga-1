#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

import random
ABC = 'abcdefghijklmÃ±opqrstuvwxyz'

def generador():
    '''
    crea palabras en base a la lista ABC
    concatena 4 elementos de ABC escogidos de manera aleatoria 
    '''
    p = ''
    p = p.join([random.choice(ABC) for i in range(4)])
    return p

def lista_random():
    '''
    aÃ±ade 5 palabras generadas en la funciÃ³n generador a 
    la lista aleatorias
    AdemÃ¡s, imprime la lista creada y la lista invertida
    '''
    a = 5
    aleatorias = []
    while a >= 0:
        palabra = generador()
        aleatorias.append(palabra)
        a -= 1
    print(aleatorias)
    aleatorias.reverse()
    print(aleatorias)
   
    

generador()
lista_random()

    
