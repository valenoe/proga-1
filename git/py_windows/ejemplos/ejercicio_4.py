#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
import random
lista = [x for x in range(1,51)]
k = random.randint(1,51)
print('lista original: ', lista,
     '\n\nk = ', k)
print()
lista1 = [x for x in lista if x == k] 
print('lista con los nÃºmeros iguales a K: ', lista1)
print()
lista2 = [x for x in lista if x < k]
print('lista con los numeros menores que K: ', lista2)
print()
lista3 = [x for x in lista if x > k]
print('lista con los numeros menores que K: ', lista3)
print()
lista4 = [x for x in lista if x%k == 0]
print('lista con los multiplos de K: ', lista4)