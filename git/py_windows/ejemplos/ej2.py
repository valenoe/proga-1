#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

alto = int(input('ingrese el alto de la matriz: '))
ancho = alto #* 2
matrix = []
for i in range(alto):
    matrix.append([])
    for j in range(ancho):
        matrix[i].append('*')
        print(matrix[i][j], end=' ')
    print()

for x in range(1, alto):
    del matrix[alto - x][alto - x]

########################################
print(matrix)
size = 6
for i in range(size):
    print('%s%s'%(' '*(size-(i-1)),'*'*((i*2)-1)))
########################################
i=1
j=5
while i<=5:
 print(i*'*'+(j*' '))
 j=j-1
 i=i+1