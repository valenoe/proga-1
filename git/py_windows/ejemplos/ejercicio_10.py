#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

def primer_paso(code):
    a  = [x for x in code if x != 'X']
    a.reverse()
    
    print('codigo secreto: ', "".join(a))
    

# main

mensaje_codificado = list("Â¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt")
print('codigo secreto codificado: ',''.join(mensaje_codificado))
primer_paso(mensaje_codificado)