#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

def primer_paso(code):
    '''tam = len(code)
    print(tam)
    a = []
    for i in range(tam):
        if code[i] != 'X':
            a.append(code[i])
            print(a)'''
    a  = [x for x in code if x != 'X']
    a.reverse()
    final = "".join(a)
    #print(a)
    print('codigo secreto: ', final)
    

# main

mensaje_codificado = list("Â¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt")
print('codigo secreto codificado: ',''.join(mensaje_codificado))
primer_paso(mensaje_codificado)