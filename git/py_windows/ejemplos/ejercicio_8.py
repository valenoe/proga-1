#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

import random

def ancho():
    N = int(input('ingrese el ancho de la matriz: '))
    return N

def imprimir(matrix, n):
    '''
    funciÃ³n que imprime la matriz llamada
    '''
    print('matriz:')
    for i in range(n):
        for j in range(n):
            print(matrix[i][j], end=' | ')
        print()
    print()

def matrix(base, n):
    '''
    funciÃ³n que crea una matriz cuadrada
    de nÃºmeros aleatorios
    '''
    for i in range(n):
        base.append([])
        for j in range(n):
            base[i].append(random.randint(0,200))
    return base

def cambio(matrix, n):
    '''
    N: reglon final de la matriz
    m: tamaÃ±o de la matriz por la mitad forzado a ser entero
    el ciclo diminuye el indice del reglon final 
    mientras aumenta el del reglon inicial
    '''
    N = n-1
    m = int(n/2)
    for k in range(m):
        aux = matrix[N-k][:]
        matrix[N-k][:] = matrix[k][:]
        matrix[k][:] = aux
    '''
    for i in range(n):
        for j in range(n):
            temp = base[i][j]
            base[i][j] = base[N][j]
            base[N][j] = temp
            '''
    return matrix

# main
base= []
n = ancho()
matrix(base, n)
imprimir(base, n)
cambio(base, n)
imprimir(base, n)