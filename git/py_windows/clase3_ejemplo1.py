#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
 
def convertir(segundos=0):
    horas = int(segundos/3600)
    segundos -= 3600*horas
    minutos = int(segundos/60)
    segundos -= 60*minutos
    return segundos, minutos, horas

horas, minutos, segundos = convertir(3601) 
print('minutos: ', minutos)
print('segundos: ', segundos)
print('horas: ', horas)