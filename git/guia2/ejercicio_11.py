#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

def ordenamiento_burbuja(lista):
    '''
    El primer ciclo cuenta desde 1 hasta el tamaÃ±o de la lista,
    guardano el dato en recorrido.
    El segundo cuenta desde 0 hasta el tamaÃ±o menos el recorrido,
    para que en cada vuelta revise la lista una vez menos que la anterior 
    '''
    z = len(lista)
    for recorrido in range(1, z):
        for i in range(z-recorrido):
            
            if lista[i] >= lista[i+1]:
                aux = lista[i]
                lista[i] = lista[i+1]
                lista[i+1] = aux
    return lista 


lista_str = ['yo', 'mando', 'aquÃ­', '786', 'manda', 'yo', 'mandoo']
lista_int = [6,5,7,8,82,1,754,14]
print(lista_str, '\n', lista_int)

ordenamiento_burbuja(lista_str)
ordenamiento_burbuja(lista_int)
print('ordenadas:\n', lista_str , '\n' ,lista_int)
