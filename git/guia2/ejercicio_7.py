#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
z = 6
def aÃ±adir_palabras(lista):
    '''
    funciÃ³n que pide al usuario ingresar palabras
    a la lista llamada 
    '''
    for x in range(z):
        a = str(input('ingrese una palabra: '))
        lista.append(a)
    return lista


def revisar(l1, l2):
    '''
    c: elementos que salen en la lista2, pero no en la lista1
    b: elementos que salen en la lista1, pero no en la lista2
    a: elementos que salen en ambas listas
    '''
    c = [s for s in l2 if s not in l1]
    b = [y for y in l1 if y not in l2]
    a = [x for x in l1 if x in l2]
    
    print('palabras en ambas listas: ', a)
    print('palabras que solo aparecen en la primera lista', b)
    print('palabras que solo aparecen en la segunda lista',c)

# main
lista1 = []
lista2 = []

print('escriba palabras para la lista 1')
aÃ±adir_palabras(lista1)
print('escriba palabras para la lista 2')
aÃ±adir_palabras(lista2)
print()
print('lista 1: ', lista1)
print('lista 2: ', lista2)
print()
revisar(lista1, lista2)