#!/usr/bin/env python3
#! -*- coding:utf-8- -*-

def concatenar(n, lista):
    '''
    aÃ±ade elementos a la lista
    '''
    z = len(n)
    for j in range(z):
        if n[j] not in lista:
            lista.append(n[j])
    return lista

# main 
'''
a, b y c se van a concatenar formando 'lista'
'nueva' es la copia de 'lista'

'''
lista = []
a = [1, 2, 3]
b = [3, 4, 5]
c = [5, 6, 7]
print('a = ', a, '\nb = ', b, '\nc = ', c)
concatenar(a, lista)
concatenar(b, lista)
concatenar(c, lista)
print(lista)