#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
frutas = {'platano': 1.35, 'manzana': 0.8, 'pera': 0.85, 'naranja':0.7}

fruta = input('que fruta vas a llevar? ').title()
kg = float(input('cuantos kilos? '))
if fruta in frutas:
    print(kg, 'kilos de', fruta, 'vale', frutas[fruta]*kg)
else:
    print('lo siento', fruta, 'no esta en el inventario')