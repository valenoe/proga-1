a = float(input('ingrese a: '))
b = float(input('ingrese b: '))
c = float(input('ingrese c: '))

discriminante = b ** 2 - 4 * a * c
if discriminante < b:
	print('la ecuacion no tiene soluciones reales')
elif discriminante == 0:
	x = -b / (2 * a)
	print('La solucion unica es x=', x)
else:
	x1 = (-b - (discriminante ** 0.5)) / (2 *a)
	x2 = (-b + (discriminante ** 0.5)) / (2 *a)
	print('las dos soluciones reales son:')
	print('x1 = ', x1)
	print('x2 = ', x2)
