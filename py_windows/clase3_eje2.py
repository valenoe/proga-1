#!/usr/bin/env python3
#! -*- coding:utf-8- -*-
try:
    # funcion que hace el trabajo de identificar el aÃ±o biciesto 
    def ultimo(year):
        if year % 4 == 0:
            if year % 100 == 0:
                print('es el ultimo aÃ±o del siglo')
                if year % 400 == 0:
                    print('es biciesto')
                else:
                    print('no es biciesto')
            else:
                print('es biciesto')
        else:
            print('no es biciesto')
# main
    year = int(input('ingrese un aÃ±o: '))
    ultimo(year)
except:
    print('algo saliÃ³ mal, escriba bien el aÃ±o')
    